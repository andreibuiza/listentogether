import React, { Component } from 'react';
import db from '../data/Database.js';
import musicClient from './Spotify.js';
import Timer from 'react-native-timer';
import RoomTrackInfo from '../data/structure/RoomTrackInfo.js';
import TrackHistoryItem from '../data/structure/TrackHistoryItem.js';
import BackgroundTimer from 'react-native-background-timer';

const AppContext = React.createContext({
    playerState: {},
    updateUserState: (newState) => {},
    roomInfo: {},
    initializeRoomInfo: (newState) => {},
    audienceListenToRoomMusic: (roomInfo) => {},
    leaveRoom: () => {},
    state: {}
});

class AppContextProvider extends Component {
    get remote() {
        return musicClient.remote;
    }

    state = {
        playerState : null,
        trackImageUri: 'https://reactnative.dev/img/tiny_logo.png',
        userInfo : {
            isDj: false,
            display_name: "",
            uri: "",
        },
        roomInfo: {
            name: "",
            dj: "",
        }, 
        spotifyAuth: {
            token: null
        },
        livePosition: 0,
        timerId: 0,
    }

    constructor(props) {
        super(props);
        this.updateUserState = this.updateUserState.bind(this)
        this.initializeRoomInfo = this.initializeRoomInfo.bind(this)
        this.updateRoomMusic = this.updateRoomMusic.bind(this)
        this.authenticateWithSpotify = this.authenticateWithSpotify.bind(this)
        this.audienceListenToRoomMusic = this.audienceListenToRoomMusic.bind(this)
        this.leaveRoom = this.leaveRoom.bind(this)
    }

    componentDidMount() {
// nothing 
    }

    componentWillUnmount() {
        Timer.clearTimeout(this);
    }

    async setTrackImageUri () {
        const re = /spotify:track:(\w+)/;
        const trackid = re.exec(this.state.playerState.track.uri)[1];
        const trackInfo =  await musicClient.getTrackInfo(trackid);
        const trackImageUri = trackInfo.album.images[0].url
        this.setState((state) => ({
           ...state,
           trackImageUri :  trackImageUri
        })) 
    }

    updateUserState(newUserState) {
        this.setState((state) => ({ ...state, userInfo: newUserState}));
    }

    clearTimeForMusicPosition = () => {
        if(this.state.timerId) {
            clearInterval(this.state.timerId);
        }
    }

    initializeLivePosition = () => {
        if (this.state.playerState.isPaused) {
            // stop the timer 
            this.clearTimeForMusicPosition();
        } else {
            // start the timer
            let timerId = setInterval(() => {
                this.setState((state) => ({ ...state, livePosition: state.livePosition + 1000}));
            }, 1000);
            this.setState((state) => ({ ...state, timerId: timerId}));
        }
    }


    updateRoomMusic = async (newRoomInfo) => {
        // this.remote.removeListener("playerStateChanged", this.updateRoomMusic);
        this.remote.removeAllListeners();

        this.clearTimeForMusicPosition();
        let roomMusicInfo = new RoomTrackInfo(newRoomInfo);
        let playerState = roomMusicInfo.get()

        if (this.state.playerState != null && newRoomInfo.track.uri != this.state.playerState.track.uri) {
            // move old song to history
            let trackHistoryItem = new TrackHistoryItem(this.state.playerState, this.state.roomInfo.score, this.state.trackImageUri);
            console.log("Saving this item")
            // console.log(trackHistoryItem.get())
            db.addSongToHistoryList(trackHistoryItem.get(), this.state.roomInfo.name);
            db.updateMusicScore(0, this.state.roomInfo.name);
        }

        this.setState((state) => ({ ...state,playerState: playerState, livePosition: playerState.playbackPosition}))
        this.setTrackImageUri()

        //Start auto timer
        this.initializeLivePosition()

        db.onUpdateMusic(this.state.playerState, this.state.roomInfo.name)

        

        // //wait for some time 0.2s and turn on the listener again
        BackgroundTimer.setTimeout(
            function() {
                this.remote.on("playerStateChanged", this.updateRoomMusic)
            }.bind(this),
            // may need to play around with this delay of subscribing to playerStateChanged again
            200
        );
    } 

    initializeRoomInfo = (newRoomInfo) => {
        return db.getRoomInfo(roomInfoState => {
            console.log("initializing room info");
            this.setState((state) => ({ ...state, roomInfo: roomInfoState}))
            //initialize music player state
            if (this.state.userInfo.isDj) {
                // subscribe to spotify app changes
                musicClient.getCurrentSong().then(currMusicInfo => {
                    this.updateRoomMusic(currMusicInfo);
                });
                
            } else {
                // get music info from DB, also subscribe to further changes
                this.audienceListenToRoomMusic(roomInfoState);
            }

            //subscribe to further changes in the room
            db.subscribeToRoomInfo((modifiedRoomInfo) => {
                // console.log("Updated Room Info:");
                // console.log(modifiedRoomInfo)
                this.setState((state) => ({ ...state, roomInfo: modifiedRoomInfo}))
            }, this.state.roomInfo.name)
        }, newRoomInfo.name);
    }

    audienceListenToRoomMusic = async (roomInfo) => {
        db.getCurrentSong(newState => {
            let playerState = newState
            this.clearTimeForMusicPosition();

            let currentTime = Date.now();
            let actualLivePosition = currentTime - playerState.timeTrackStarted;
            if (actualLivePosition > playerState.track.duration) {
                actualLivePosition = 0;
                playerState.isPaused = true;
            }
            playerState.playbackPosition = actualLivePosition;

            this.setState((state) => ({ ...state,playerState: playerState, livePosition: actualLivePosition}))
            this.setTrackImageUri()
            this.initializeLivePosition()
            if(!this.state.playerState.isPaused) {
                musicClient.playSong(this.state.playerState.track.uri, this.state.playerState.playbackPosition);
            } else {
                musicClient.pauseMusic();
            }
        }, roomInfo.name);
    }

    leaveRoom() {
        this.remote.removeAllListeners();
        db.disconnectSong(this.state.roomInfo.name);
        db.disconnectRecommendation(this.state.roomInfo.name);
        db.disconnectRoomInfoSub(this.state.roomInfo.name);
        db.removeUserFromRoom(this.state.roomInfo.name, this.state.userInfo);    
        this.clearTimeForMusicPosition();
    }

    async authenticateWithSpotify() {
        try{
            const session = await musicClient.authenticate();
            this.setState((state) => ({ ...state, spotifyAuth: {token: session.accessToken}}));
            await musicClient.connectSpotifyRemote(this.state.spotifyAuth.token);
            let newUserInfo = await musicClient.getUserInfo();
            newUserInfo.isDj = false;
            this.setState((state) => ({ ...state, userInfo: newUserInfo}));
            console.log("User:")
            console.log(this.state.userInfo);
            return Promise.resolve(true);
        } catch(err) {
            console.error("Couldn't authorize with or connect to Spotify",err);
        }
        return Promise.resolve(false);
    }

    render() {
        const { children } = this.props
        return (
            <AppContext.Provider
                value={{
                    playerState : this.state.playerState,
                    updateUserState: this.updateUserState,
                    roomInfo: this.state.roomInfo,
                    initializeRoomInfo: this.initializeRoomInfo,
                    auth: this.auth,
                    authenticateWithSpotify: this.authenticateWithSpotify,
                    audienceListenToRoomMusic: this.audienceListenToRoomMusic,
                    leaveRoom: this.leaveRoom,
                    state: this.state
                }}
            >
                {children}
            </AppContext.Provider>
        )
    }
}

export default AppContext;
export {AppContextProvider};