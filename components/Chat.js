import React, { Component } from 'react';
import { View, StyleSheet, Button } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
import { renderInputToolbar, renderActions, renderComposer, renderSend } from './InputToolbar';
import db from '../data/Database.js';
import AppContext from './AppContext';
import styles from './Styles';
import musicClient from './Spotify';
import update from 'immutability-helper';
import TrackRecommendationItem from '../data/structure/TrackRecommendationItem';


const recommendRegex = "#recommend https:\/\/open\.spotify\.com\/track\/(\w+)";

class Chat extends Component {
  context = this.context
  state = {
    messages: [],
  };

  listenTogetherUser = {
    name: "ListenTogther",
    _id: "ListenTogther"
  }

  get roomName() {
    return this.context.roomInfo.name;
  }

  get user() {
    return { 
      _id: this.context.state.userInfo.id,
      name: this.context.state.userInfo.display_name,
      avatar: this.context.state.userInfo.images[0].url
    };
  }

  get listenButtonTitle() {
    if (this.user.name == "admin") {
      return "Play Music";
    } else {
      return "Listen Now";
    } 
  }

  async parseRecommend(messages) {
    for (const message of messages) {
      console.log(message.text)
      if(!message.text.startsWith("#recommend")) {
        continue;
      }
      const re = /#recommend https:\/\/open\.spotify\.com\/track\/(\w+)/;
      let thanksForReceommendStr=""
      let results = re.exec(message.text);
      if (results != null) {
        let trackid = results[1]
        let trackInfo = await musicClient.getTrackInfo(trackid);
        console.log("User is recommending " + trackInfo.name);
        let recommendationItem = new TrackRecommendationItem(trackInfo, this.user);
        // TODO save the recommendation if it does not exist yet
        db.saveSongRecommendation(recommendationItem.get(), this.roomName);
        // TODO add all artists
        thanksForReceommendStr = "Thank you for recommending " 
          + trackInfo.name + ", by " + trackInfo.artists[0].name
      } else {
        thanksForReceommendStr = "There was something wrong with the recommendation link"
      }
      this.addUnsavedMessage(thanksForReceommendStr);
    }
  }

  parseMessage = (messages) => {
    db.onSend(messages, this.roomName)
    this.parseRecommend(messages)
  }

  addUnsavedMessage = (msg) => {
    const timestamp = new Date();
    const message = {
      _id: 1,
      timestamp,
      text: msg,
      system: true
    };
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, message),
    }))
  }

  render() {
    
    return (
      <GiftedChat
          messages={this.state.messages}
          onSend={this.parseMessage}
          user={this.user}
          parsePatterns={(linkStyle) => [
            { pattern: /#(\w+)/, style: styles.hashTag},
          ]} 
      />
    );
  };

  componentDidMount() {
    if (this.context.state.userInfo.isDj) {
      this.addUnsavedMessage("Welcome to " + this.roomName + "!!!");
    }
    db.getMessages(
      message => {
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, message),
        }))
      },
      this.roomName
    );
  }
  componentWillUnmount() {
    db.disconnectRoom(this.roomName);
  }
} 
Chat.contextType = AppContext;

export default Chat;
