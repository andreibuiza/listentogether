import React, {useState} from 'react';

import {
    View,
    Button,
    Text,
    Toast,
    Colors
} from 'react-native-ui-lib';
import { Linking } from "react-native";

  
import styles from './Styles.js';

export default function LinkToSpotify (uri) {

    const [showToast, setToastVisiblity] = useState(false);

    const openInSpotify = async () => {
        let spotifyUrl = uri;
        const spotifyInstalled = await Linking.canOpenURL(spotifyUrl);
        if (spotifyInstalled) {
            await Linking.openURL(spotifyUrl);
        } else {
            setToastVisiblity(true);
        }
    }

    const spotifyNotInstalledMessage = "Please install Spotify from the app store";
    const dismissTopToast = () => {
        setToastVisiblity(false)
    }
    return (
        <View>
          <Button large rounded iconLeft info
            style={styles.genericButton}
            onPress={openSpotify}>
              <Text>Open Spotify</Text>
          </Button>
            <Toast
                visible={showToast}
                position={'Top'}
                message={spotifyNotInstalledMessage}
                onDismiss={dismissTopToast}
                showDismiss={true}
                backgroundColor = {Colors.red30}
            />
        </View>
    )
}