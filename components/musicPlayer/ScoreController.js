import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Feather } from '@expo/vector-icons'; 
import React, {useState} from 'react';

const styles = StyleSheet.create({
    score: {
      fontSize: 12, 
      color: "gray" 
    },
  });

export default function ScoreController({ score, updateScore }) {
    const [currentScore, setScore] = useState(score);

    const onPress = () => {
        let newScore = currentScore + 1;
        setScore(newScore);
        console.log("new score:" + newScore);
        updateScore(newScore);
    }

    return (
        <View>  
            <Text style={styles.score}>{score}</Text>
            <TouchableOpacity onPress={onPress}>
                <Feather name="heart" size={18} color="black" />
            </TouchableOpacity>
        </View>
      );
}