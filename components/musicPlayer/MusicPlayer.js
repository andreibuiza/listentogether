import React, { Component } from 'react';
import musicClient from '../Spotify.js';
import Slider from '@react-native-community/slider';

import { Duration } from 'luxon';
import { RepeatMode
} from 'react-native-spotify-remote';
import { Button, Switch, Segment, Icon } from 'native-base';
import AppContext from '../AppContext';
import styles from '../Styles';
import { TouchableOpacity, StyleSheet, View, Text, Image } from 'react-native';
import { Feather } from '@expo/vector-icons';
import ScoreController from './ScoreController';



class MusicPlayer extends Component {
    context = this.context
    
    constructor() {
        super()
    }

    get remote() {
        return musicClient.remote;
    }

    get trackImageUri() {
        return this.context.state.trackImageUri;
    }
  get artistName() {
    return this.context.playerState.track.artist.name;
  }

  get duration() {
        return this.context.playerState.track.duration;
  }

  get canRepeatContext() {
    return this.context.playerState.playbackRestrictions.canRepeatContext;
  }

  get canRepeatTrack() {
      return this.context.playerState.playbackRestrictions.canRepeatTrack;
  }
  get canToggleShuffle() {
      return this.context.playerState.playbackRestrictions.canToggleShuffle;
  }

  get isShuffling() {
      return this.context.playerState.playbackOptions.isShuffling;
  }

  get repeatMode() {
        return this.context.playerState.playbackOptions.repeatMode;
  }

  get isPaused() {
    return this.context.playerState.isPaused;
  }

  displayDuration(durationMs) {
    const d = Duration.fromMillis(durationMs);
    return d.hours > 0 ?
      d.toFormat("h:mm:ss")
      : d.toFormat("m:ss");
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }
  onError() {
      console.log("failure!");
  }

  isDj = () => {
    return this.context.state.userInfo.isDj;
    }

    joinRoomMusic = () => {
        console.log("Joining room music")
        this.context.audienceListenToRoomMusic(this.context.roomInfo);
    }

    renderListenNow = () => {
        return (
            <>
                {this.isDj() ? 
                <></>
                     :
                    <TouchableOpacity
                        onPress = {this.joinRoomMusic}
                        >
                        <View
                            style = {musicPlayerStyles.refresh}>
                            <Feather name="refresh-cw" size={40} color="black" />
                        </View>
                    </TouchableOpacity>
                }
            </>
          );
    }

  render() {
    const buttons = [
        {
            content: <Icon name="md-skip-backward"/>,
            action: async () => await this.remote.skipToPrevious(),
        },
        {
            content: <Icon name="md-skip-forward"/>,
            action: async () => await this.remote.skipToNext(),
        }
    ];
    // Put play/pause into the middle of the array
    buttons.splice(buttons.length / 2, 0, this.isPaused ?
        {
            content: <Icon name="play"/>,
            action: async () => await this.remote.resume()
        }
        :
        {
            content: <Icon name="pause"/>,
            action: async () => await this.remote.pause()
        }
    );

    const updateCurrentMusicScore = (updatedScore) => {
        db.updateMusicScore(updatedScore, this.context.roomInfo.name);
    }

    return(
        <View style={styles.transportContainer}>
                <View style={{ display: "flex", alignItems: "center", justifyContent: "center", marginVertical: 10 }}>
                    <Image
                        style={musicPlayerStyles.trackImage}
                        source={{uri: this.trackImageUri}}
                    />
                    <Text style={{ fontSize: 20, fontWeight: "500" }}>{this.context.playerState.track.name}</Text>
                    {this.artistName !== "" && <Text style={{ fontSize: 17, fontWeight: "300" }}>{this.artistName}</Text>}
                </View>

                <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                    <Text style={[styles.textDuration, styles.textCentered]}>{this.displayDuration(this.context.state.livePosition)}</Text>
                    <Slider
                        minimumValue={0}
                        maximumValue={this.duration}
                        value={this.context.state.livePosition}
                        onSlidingComplete={(val) => {
                            this.remote.seek(Math.round(val));
                        }}
                        style={{ flex: 1 }}
                    />
                    <Text style={[styles.textDuration, styles.textCentered]}>{this.displayDuration(this.duration)}</Text>
                </View>
                {this.isDj() ?
                    <>
                        <View>
                            <Segment>
                                {buttons.map(({ content, action }, index) => (
                                    <Button
                                        key={`${index}`}
                                        onPress={() => action().catch(this.onError)}
                                        transparent
                                        style={{
                                            width: `${(100 / buttons.length)}%`,
                                            height: 60,
                                            backgroundColor: "#FFF",
                                            borderLeftWidth: index === 0 ? 1 : 0,
                                            display:'flex',
                                            justifyContent:'center'
                                        }}
                                    >
                                        {content}
                                    </Button>
                                ))}
                            </Segment>
                            <View style={{
                                marginVertical: 30,
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                alignItems: 'center'
                                }}>
                                <View style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        justifyContent: "space-between",
                                        height: 50
                                    }}>
                                        <Switch
                                        disabled={!this.canToggleShuffle}
                                        value={this.isShuffling} 
                                        onValueChange={(val) => {
                                            this.remote.setShuffling(val);
                                        }}
                                        />
                                        <Text>Shuffle</Text>
                                </View>
                                <Button
                                    disabled={!this.canRepeatContext && !this.canRepeatTrack}
                                    onPress={() => {
                                        const newMode = (this.repeatMode + 1) % 3;
                                        this.remote.setRepeatMode(newMode);
                                    }}
                                    rounded
                                >
                                    <Text style={{ width: 150, textAlign: "center" }}>
                                        {this.repeatMode === RepeatMode.Off && "Repeat Off"}
                                        {this.repeatMode === RepeatMode.Track && "Repeat One"}
                                        {this.repeatMode === RepeatMode.Context && "Repeat Context"}
                                    </Text>
                                </Button>
                            </View>
                        </View>
                    </>
                    :
                    <>
                    </>
                }
                {this.renderListenNow()}
                <ScoreController score={this.context.roomInfo.score} updateScore={updateCurrentMusicScore}/>


        </View>
    )
  }
}
MusicPlayer.contextType = AppContext;

export default MusicPlayer;

const musicPlayerStyles = StyleSheet.create({
    refresh: {
        justifyContent: 'center',
        alignItems: 'center',
        display: "flex"
    },
    trackImage: {
        width: 240,
        height: 230,
    }
})
