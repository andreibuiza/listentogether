import styles, {spotifyIcon, heartIcon, recommendIcon} from './Styles';
import React, {useState}from 'react';
import { TouchableOpacity } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import {View, Text, ActionSheet, Card, Colors, Image} from 'react-native-ui-lib';
import {openInSpotify, spotifyLike, recommend} from './DefaultItemMethods';


export default function TrackSearchResultItem({ trackInfo, context }) {
    const [showOptions, setShowOptions] = useState(false);

    const imageSource = trackInfo.album.images[trackInfo.album.images.length - 1].url;
    return (
        <View>
            <Card
                row
                style={styles.card}
                enableBlur
                useNative
                backgroundColor={Colors.white}
                activeOpacity={1}
                activeScale={true ? 0.96 : 1.04}
            >
                <Image
                    style={styles.image}
                    source={{uri: imageSource}}
                />
                <View margin-20 flex centerV>
                <TouchableOpacity onPress={openInSpotify}>
                    <Text text70 dark10>
                        {trackInfo.name}
                    </Text>
                    <Text text70 dark40>
                        {/* TODO include all the artists here */}
                        {trackInfo.artists[0].name}
                    </Text>
                </TouchableOpacity>
                </View>
                <View marginR-10 centerV flex right>
                    <TouchableOpacity onPress={() => setShowOptions(true)}>
                        <Entypo name="dots-three-vertical" size={18} color="black" />
                    </TouchableOpacity>
                </View>
            </Card>

            <ActionSheet
                renderTitle={() => {
                return (
                    <View marginT-20 centerH>
                    <Image
                        style={styles.image}
                        source={{uri: imageSource}}
                    />
                    <Text dark10 text70 >
                        {trackInfo.name}
                    </Text>
                    </View>
                );
                }}
                options={[
                { label: 'Open on Spotify', onPress: () => openInSpotify(trackInfo.album.uri), icon: spotifyIcon },
                { label: 'Add to Liked Songs', onPress: () => spotifyLike(trackInfo.id), icon: heartIcon},
                { label: 'Recommend', onPress: () => recommend(trackInfo, context), icon: recommendIcon},
                // TODO: preview song
                ]}
                visible={showOptions}
                onDismiss={() => setShowOptions(false)}
            />
        </View>
        
    )
}