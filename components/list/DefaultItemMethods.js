import musicClient from '../Spotify';
import { Linking} from "react-native";
import TrackRecommendationItem from '../../data/structure/TrackRecommendationItem';
import db from '../../data/Database';


// Available actions
export const openInSpotify = async (uri) => {
    let spotifyUrl = uri;
    console.log("opening: " + spotifyUrl);
    const spotifyInstalled = await Linking.canOpenURL(spotifyUrl);
    if (spotifyInstalled) {
        await Linking.openURL(spotifyUrl);
    } else {
        // TODO display a toast if spotify not installed.
    }
}
export const spotifyLike = (id) => {
    musicClient.addTrackToLikedSongs(id);
}

export const recommend = (trackToRecommend, context) => {
    let recommendInfo = new TrackRecommendationItem(trackToRecommend, context.state.userInfo);
    db.saveSongRecommendation(recommendInfo.get(), context.roomInfo.name);
}