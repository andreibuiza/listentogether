import React, { Component } from 'react';
import AppContext from '../AppContext';
import RecommendItem from './RecommendItem';  
import Constants from 'expo-constants';
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';
import update from 'immutability-helper';
import musicClient from '../Spotify.js';


class RecommendList extends Component {
    context = this.context

    state = {
        // TODO is it ok to have recommendations list as a state and not saved in app context?
        recommendations: new Map(),
    };  

    componentDidMount() {
        db.listenToRecommendationChanges(
            (r) => this.updateRecommendationState(r, false),
            this.context.roomInfo.name
          );
        db.listenToRecommendationDelete(
            (r) => this.updateRecommendationState(r, true),
            this.context.roomInfo.name
          );
    }

    updateRecommendationState = (r, isDelete) => {
        const newState = (isDelete)? update(this.state, 
            {
                recommendations: {$remove: [r.key]},
            }) 
            : update(this.state, 
            {
                recommendations: {$add: [[r.key, {recommendation: r.recommendation, key: r.key}]]},
            });
        
        this.setState(newState);
    }

    updateRecommendation = (room) => {
        return (updatedRecommendation, id) => {
            db.updateRecommendation(updatedRecommendation, room, id);
        }
    }

    playRecommendation(room) {
        return (recommendation, id) => {
            console.log("Playing song recommendation spotify:track:" + recommendation.id);
            musicClient.playSong("spotify:track:" + recommendation.id, 0);
            db.deleteRecommendation(room,id);
        }
    }

    queueRecommendation(room) {
        return (recommendation, id) => {
            console.log("Playing song recommendation spotify:track:" + recommendation.id);
            musicClient.queueSong("spotify:track:" + recommendation.id, 0);
            db.deleteRecommendation(room,id);
        }
    }   

    isDj = () => {
        return this.context.state.userInfo.isDj;
    }


    render() {
        const onPress = {
                update : this.updateRecommendation(this.context.roomInfo.name),
                play: this.playRecommendation(this.context.roomInfo.name),
                queue: this.queueRecommendation(this.context.roomInfo.name)
        }
        return(
            <View>
                <FlatList
                    data={Array.from(this.state.recommendations.values())}
                    renderItem={({ item }) => 
                        <RecommendItem item={item}    
                        onPress = {
                            onPress
                        }
                        isDj = {this.isDj()} />
                    }
                    keyExtractor={item => item.id}
                    extraData={this.state}
                />
            </View> 
        )
    }
}
RecommendList.contextType = AppContext;
   
export default RecommendList;
