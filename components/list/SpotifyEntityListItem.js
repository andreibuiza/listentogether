import React, {useState} from 'react';
import {
    Colors,
    View,
    Card,
    Text,
    Image,
    ActionSheet
  } from 'react-native-ui-lib';
import { Linking, TouchableOpacity, StyleSheet } from "react-native";
import { Entypo } from '@expo/vector-icons';
import musicClient from '../Spotify';
import db from '../../data/Database';
import TrackRecommendationItem from '../../data/structure/TrackRecommendationItem';

import styles from '../Styles.js';

const defaultImageHeight = 80;
const defaultImageWidth = defaultImageHeight;
const defaultItemCard = {
  height: defaultImageHeight,
  marginBottom: 15,
  marginLeft: 10,
  marginRight: 10
}
const defaultTrackItem = StyleSheet.create( {
  card : defaultItemCard,
  image : {
    width: defaultImageHeight, 
    height: defaultImageWidth
  }
});
const defaultUserItem = StyleSheet.create({
  card : defaultItemCard,
  image : {
    width: defaultImageHeight, 
    height: defaultImageWidth,
    borderRadius: defaultImageHeight / 2
  }
});

const spotifyIcon = require('../../assets/spotify-icons-logos/icons/01_RGB/02_PNG/Spotify_Icon_RGB_Green.png')
const heartIcon = require('../../assets/heart.png')
const recommendIcon = require('../../assets/poll.png')
export default function SpotifyEntityListItem ({itemInfo, itemStyle, actionComponent, context}) {

    const [showUserOptions, setShowUserOptions] = useState(false);
    const [showSearchTrackOptions, setShowSearchTrackOptions] = useState(false);

    const trackInfo = {
      id: "127QTOFJsJQp5LbJbu3A1y",
      image : "https://i.scdn.co/image/ab67616d00001e02bba7cfaf7c59ff0898acba1f",
      name: "Toosie Slide",
      artist: "Drake",
      artists: [{name: "Drake"}],
      searchResult : {

      },

    }

    const userInfo = {
      name: "Aaron Buiza",
      imageSource : "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=10152514067013072&height=300&width=300&ext=1600972338&hash=AeSo-i5NezzAixBt"
    }

    const showDotsOptions = (showOptions) => {
      return(
        <View marginR-10 centerV flex right>
          <TouchableOpacity onPress={() => showOptions(true)}>
            <Entypo name="dots-three-vertical" size={18} color="black" />
          </TouchableOpacity>
        </View>
      )
    }
    // Available actions
    const openSpotify = async () => {
        let spotifyUrl = "spotify:";
        console.log("opening spotify!")
        await Linking.openURL(spotifyUrl);
    }

    const voteUp = () => {

    }

    const spotifyLike = (id) => {
      musicClient.addTrackToLikedSongs(id);
    }

    const recommend = (trackToRecommend) => {
      let recommendInfo = new TrackRecommendationItem(trackToRecommend, context.state.userInfo);
      db.saveSongRecommendation(recommendInfo.get(), context.roomInfo.name);
    }

    return (
        // <View>
        //     <Image
        //         style={itemStyle.trackImage}
        //         source={{uri: itemInfo.imageUri}}
        //     />
        //     <Text style={itemStyle.name}>{itemInfo.name}</Text>
        //     { itemInfo.type != "artist" && itemInfo.type != "user" ?
        //         <>
        //         <Text style={itemStyle.artists}>{trackInfo.artists}</Text>

        //         </>
        //         :
        //         <>
        //         </>
        //     }
        // </View>

        <View marginT-10>
          {/* User - Who's here? action: open on Spotify*/}
          <Card
            row
            style={defaultUserItem.card}
            enableBlur
            borderRadius={60}
            useNative
            backgroundColor={Colors.white}
            activeOpacity={1}
            activeScale={true ? 0.96 : 1.04}
          >
            <Image
                style={defaultUserItem.image}
                source={{uri: userInfo.imageSource}}
            />
            <View margin-20 flex centerV>
              <TouchableOpacity onPress={openSpotify}>
                <Text text70 dark10>
                    {userInfo.name}
                </Text>
              </TouchableOpacity>
            </View>
            {showDotsOptions(() => setShowUserOptions(true))}
          </Card>

          <ActionSheet
            renderTitle={() => {
              return (
                <View marginT-20 centerH>
                  <Image
                      style={defaultUserItem.image}
                      source={{uri: userInfo.imageSource}}
                  />
                  <Text dark10 text70 >
                    {userInfo.name}
                  </Text>
                </View>
              );
            }}
            options={[
              { label: 'Open on Spotify', onPress: () => openSpotify(), icon: spotifyIcon  },
            ]}
            visible={showUserOptions}
            onDismiss={() => setShowUserOptions(false)}
          />

          {/* // Track - recommendation item. action: preview, open on Spotify, like, clap w/ score */}
          <Card
            row
            style={defaultTrackItem.card}
            enableBlur
            useNative
            backgroundColor={Colors.white}
            activeOpacity={1}
            activeScale={true ? 0.96 : 1.04}
          >
            <Image
                style={defaultTrackItem.image}
                source={{uri: trackInfo.image}}
            />
            <View margin-20 flex centerV>
              <TouchableOpacity onPress={openSpotify}>
                <Text text70 dark10>
                    {trackInfo.name}
                </Text>
                <Text text70 dark40>
                    {trackInfo.artist}
                </Text>
              </TouchableOpacity>
            </View>
            <View>
              
            </View>
            {showDotsOptions(() => setShowSearchTrackOptions(true))}
          </Card>

          <ActionSheet
            renderTitle={() => {
              return (
                <View marginT-20 centerH>
                  <Image
                      style={defaultTrackItem.image}
                      source={{uri: trackInfo.image}}
                  />
                  <Text dark10 text70 >
                    {trackInfo.name}
                  </Text>
                </View>
              );
            }}
            options={[
              { label: 'Open on Spotify', onPress: () => openSpotify(), icon: spotifyIcon },
              { label: 'Add to Liked Songs', onPress: () => spotifyLike(trackInfo.id), icon: heartIcon},
              // TODO: preview song
            ]}
            visible={showSearchTrackOptions}
            onDismiss={() => setShowSearchTrackOptions(false)}
          />

          {/* // Track - history item. action: preview, open on Spotify, like, recommend(?)*/}
          {/* // Track - search result. action: preview, open on Spotify, like, recommend */}
          <Card
            row
            style={defaultTrackItem.card}
            enableBlur
            useNative
            backgroundColor={Colors.white}
            activeOpacity={1}
            activeScale={true ? 0.96 : 1.04}
          >
            <Image
                style={defaultTrackItem.image}
                source={{uri: trackInfo.image}}
            />
            <View margin-20 flex centerV>
              <TouchableOpacity onPress={openSpotify}>
                <Text text70 dark10>
                    {trackInfo.name}
                </Text>
                <Text text70 dark40>
                    {trackInfo.artist}
                </Text>
              </TouchableOpacity>
            </View>
            {showDotsOptions(() => setShowSearchTrackOptions(true))}
          </Card>

          <ActionSheet
            renderTitle={() => {
              return (
                <View marginT-20 centerH>
                  <Image
                      style={defaultTrackItem.image}
                      source={{uri: trackInfo.image}}
                  />
                  <Text dark10 text70 >
                    {trackInfo.name}
                  </Text>
                </View>
              );
            }}
            options={[
              { label: 'Open on Spotify', onPress: () => openSpotify(), icon: spotifyIcon },
              { label: 'Add to Liked Songs', onPress: () => spotifyLike(trackInfo.id), icon: heartIcon},
              { label: 'Recommend', onPress: () => recommend(trackInfo), icon: recommendIcon},
              // TODO: preview song
            ]}
            visible={showSearchTrackOptions}
            onDismiss={() => setShowSearchTrackOptions(false)}
          />

          {/* FUTURE. richer experience by being able to search other types of entities */}
          
          {/* // Album - search result. action: open on Spotify */}

          {/* // Artist - search result. action: open on Spotify*/}


        </View>

        // History - just like a track but with Preview and Save to Spotify Options


        // Album
    )
}
