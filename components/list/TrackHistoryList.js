import React, { Component } from 'react';
import TrackHistoryItem from './TrackHistoryItem';
import AppContext from '../AppContext';
import {  View, FlatList, Text } from 'react-native';


class TrackHistoryList extends Component {
    context = this.context

    get songHistory() {
        return Object.keys(this.context.roomInfo.songHistory).map(key => this.context.roomInfo.songHistory[key])
    }

    render() {
        return(
            <View>
                <FlatList
                    data={Array.from(this.songHistory)}
                    renderItem={({ item }) => 
                        <TrackHistoryItem trackHistoryItem={item}/>
                    }
                    keyExtractor={item => item.id}
                />
            </View> 
        )
    }
}

TrackHistoryList.contextType = AppContext;

export default TrackHistoryList;

