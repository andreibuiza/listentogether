import React, {useState} from 'react';
import { TouchableOpacity } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import {View, Text, ActionSheet, Card, Colors, Image} from 'react-native-ui-lib';
import { FontAwesome5 } from '@expo/vector-icons'; 
import styles, {spotifyIcon, heartIcon} from './Styles';
import {openInSpotify} from './DefaultItemMethods';

  const recommendStatus =  new Set(['Ok', 'Maybe', 'Nope']);

  const isStatus = (str) => {
    return recommendStatus.has(str);
  }

export default function RecommendItem({ item, onPress, isDj }) {

  const [showRecommendTrackOptions, setShowRecommendTrackOptions] = useState(false);

  const trackInfo = item.recommendation;

  const updateVote = () => {
    trackInfo.vote = trackInfo.vote + 1 
    return onPress.update(trackInfo, item.key)
  };

  const optionPressed = (str) => {
    if(isStatus(str)) {
      trackInfo.status = str
      return onPress.update(trackInfo, item.key)
    } 
    if('Queue' == str) {
      return onPress.queue(trackInfo, item.key)
    }
    if('Play' == str) {
      return onPress.play(trackInfo, item.key)
    }
  }

  const djOptions = [
    { label: 'Open on Spotify', onPress: () => openInSpotify(trackInfo.albumUri), icon: spotifyIcon },
    { label: 'Add to Liked Songs', onPress: () => spotifyLike(trackInfo.id), icon: heartIcon},
    { label: 'Play', onPress: () => optionPressed("Play") },
    { label: 'Queue', onPress: () => optionPressed("Queue") },
    { label: 'Ok', onPress: () => optionPressed('Ok') },
    { label: 'Maybe', onPress: () => optionPressed('Maybe')},
    { label: 'Nope', onPress: () => optionPressed('Nope')},
  ]

  const audienceOptions = [
    { label: 'Open on Spotify', onPress: () => openInSpotify(trackInfo.albumUri), icon: spotifyIcon },
    { label: 'Add to Liked Songs', onPress: () => spotifyLike(trackInfo.id), icon: heartIcon}, ]

    return (
      <View marginT-10>
        <Card
            row
            style={styles.card}
            enableBlur
            useNative
            backgroundColor={Colors.white}
            activeOpacity={1}
            activeScale={true ? 0.96 : 1.04}
          >
            <Image
                style={styles.image}
                source={{uri: trackInfo.image}}
            />
            <View margin-20 flex centerV>
              <TouchableOpacity onPress={openInSpotify}>
                <Text text70 dark10>
                    {trackInfo.name}
                </Text>
                <Text text70 dark40>
                    {trackInfo.artist.name}
                </Text>
              </TouchableOpacity>
            </View>
            <View marginR-10 centerV flex right row>
              <View row marginR-20>
                <Text marginR-20 style={styles.songStatus}>{trackInfo.status}</Text>
                <Text marginR-20 style={styles.songStatus}>{trackInfo.vote}</Text>
                <TouchableOpacity onPress={updateVote}>
                  <FontAwesome5 name="praying-hands" size={18} color="black" />
                </TouchableOpacity>
              </View>
              <TouchableOpacity onPress={() => setShowRecommendTrackOptions(true)}>
                <Entypo name="dots-three-vertical" size={18} color="black" />
              </TouchableOpacity>
            </View>
          </Card>

          <ActionSheet
            renderTitle={() => {
              return (
                <View marginT-20 centerH>
                  <Image
                      style={styles.image}
                      source={{uri: trackInfo.image}}
                  />
                  <Text dark10 text70 >
                    {trackInfo.name}
                  </Text>
                </View>
              );
            }}
            options={isDj? djOptions:audienceOptions}
            visible={showRecommendTrackOptions}
            onDismiss={() => setShowRecommendTrackOptions(false)}
          />
      </View>

    );
}