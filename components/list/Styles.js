import { StyleSheet} from 'react-native';
import Constants from  'expo-constants';

export const spotifyIcon = require('../../assets/spotify-icons-logos/icons/01_RGB/02_PNG/Spotify_Icon_RGB_Green.png')
export const heartIcon = require('../../assets/heart.png')
export const recommendIcon = require('../../assets/poll.png')

const defaultImageHeight = 80;
  const defaultImageWidth = defaultImageHeight;
  const defaultItemCard = {
    height: defaultImageHeight,
    marginBottom: 15,
    marginLeft: 10,
    marginRight: 10
  }

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: Constants.statusBarHeight,
    },
    songTitle: {
      fontSize: 18,
    },
    songArtists: { 
      width: "100%", 
      marginTop: 5, 
      fontSize: 12, 
      color: "gray" 
    },
    songStatus: {
      fontSize: 12, 
      color: "gray" 
    },
    card: defaultItemCard,
    image: {
        width: defaultImageWidth,
        height: defaultImageWidth
    }
  });

export default styles;