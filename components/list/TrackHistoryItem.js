import React, {useState}from 'react';
import { TouchableOpacity } from 'react-native';
import styles, {spotifyIcon, heartIcon} from './Styles';
import {View, Text, ActionSheet, Card, Colors, Image} from 'react-native-ui-lib';
import { Entypo } from '@expo/vector-icons';
import {openInSpotify} from './DefaultItemMethods';


export default function TrackHistoryItem({ trackHistoryItem }) {
const [showOptions, setShowOptions] = useState(false);

  const trackInfo = trackHistoryItem;

  const options = [
    { label: 'Open on Spotify', onPress: () => openInSpotify(trackHistoryItem.albumUri), icon: spotifyIcon },
    { label: 'Add to Liked Songs', onPress: () => spotifyLike(trackInfo.id), icon: heartIcon}, ]

  const spotifyLike = (id) => {
    musicClient.addTrackToLikedSongs(id);
  }

    return (
      <View marginT-10>
        <Card
            row
            style={styles.card}
            enableBlur
            useNative
            backgroundColor={Colors.white}
            activeOpacity={1}
            activeScale={true ? 0.96 : 1.04}
          >
            <Image
                style={styles.image}
                source={{uri: trackInfo.image}}
            />
            <View margin-20 flex centerV>
              <TouchableOpacity onPress={openInSpotify}>
                <Text text70 dark10>
                    {trackInfo.name}
                </Text>
                <Text text70 dark40>
                    {trackInfo.artist}
                </Text>
              </TouchableOpacity>
            </View>
            <View marginR-10 centerV flex right row>
              <TouchableOpacity onPress={() => setShowOptions(true)}>
                <Entypo name="dots-three-vertical" size={18} color="black" />
              </TouchableOpacity>
            </View>
          </Card>

          <ActionSheet
            renderTitle={() => {
              return (
                <View marginT-20 centerH>
                  <Image
                      style={styles.image}
                      source={{uri: trackInfo.image}}
                  />
                  <Text dark10 text70 >
                    {trackInfo.name}
                  </Text>
                </View>
              );
            }}
            options={options}
            visible={showOptions}
            onDismiss={() => setShowOptions(false)}
          />
      </View>

    );
}
