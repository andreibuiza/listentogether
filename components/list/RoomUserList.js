import React, { Component } from 'react';
import RoomUserItem from './RoomUserItem';
import AppContext from '../AppContext';
import {  View, FlatList, Text } from 'react-native';


class RoomUserList extends Component {
    context = this.context

    get userList() {
        return Object.keys(this.context.roomInfo.users).map(key => this.context.roomInfo.users[key])
    }

    render() {
        return(
            <View>
                <FlatList
                    data={Array.from(this.userList)}
                    renderItem={({ item }) => 
                        <RoomUserItem userItem={item}/>
                    }
                    keyExtractor={item => item.id}
                />
            </View> 
        )
    }
}

RoomUserList.contextType = AppContext;

export default RoomUserList;

