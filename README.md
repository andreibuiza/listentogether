## Development with Metro Bundler

Build and install dev build on Android
```
yarn android
```


## Test install without Metro Bundler

Build and install test build for Android platform. The build process requires a running Metro Bundle server. When the package is installed on the Android device, it can run on its own.
```
cd android
./gradlew bundleRelease
```
Open Android Studio to `android/` of the project.
Select Build Variants, and set `release` as the Active Build variant


## Building the project on a new machine

If this project is being built using a different machine, it's required to setup your own Spotify Project under your [Dashboard](https://developer.spotify.com/dashboard/applications)

1) Go to Dashboard main page

2) Create an App

3) Fill in the details

4) Once created, go to Edit Settings

5) Add Application Name

6) Android Packages: "com.listentogether", SHA1 (`cd android; ./gradlew signingReport` look for Task :app:signingReport's SHA1) 

7) Go to file `Spotify.js` and change Client ID with the value you see from your Spotify App


## Setup your debug and upload keystore

1) `cd $JAVA_HOME`

2) ` sudo /bin/keytool -genkey -v -keystore <NAME_OF_KEYSTORE>.keystore -alias listenTogether -keyalg RSA -keysize 2048 -validity 10000`

3) `mv <NAME_OF_KEYSTORE>.keystore $LISTEN_TOGETHER_DIR/android/app`

4) double check that `gradle.properties` has correct values for the keystore variables