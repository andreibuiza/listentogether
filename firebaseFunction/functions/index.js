const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();
var database = admin.database();
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

exports.updateRoomTrackInfo = functions.database.instance('basicchat-21c35').ref('/queue/{roomName}').onCreate((snap, context) => {
    functions.logger.log('New room: ' + context.params.roomName + " listener activated", context.params.roomName);
    return updateRoomTrack(context.params.roomName);
})


waitForTrackToFinish = (duration, onFinish) => {
    setTimeout(() => {
        onFinish();
    }, duration);
}

updateRoomTrack = async (roomName) => {
    let trackQueue = await get(queuePath(roomName));
    trackQueue = trackQueue.val()
    if (!trackQueue) {
        return;
    }

    // if queue is empty, delete /queue/roomName
    if (trackQueue.length == 0){
        deleteP(queuePath(roomName));
    }

    let roomTrack = await get(roomTrackPath(roomName));
    roomTrack = roomTrack.val();
    functions.logger.log('roomTrack ' + roomName);
    functions.logger.log(roomTrack);
    if (roomTrack) {
        functions.logger.log('Updating track history list of ' + roomName);
        functions.logger.log(roomTrack);
        await addSongToHistoryList(roomTrack, roomName);
    }    

    let newTrack = trackQueue.shift();

    let duration = 10000; //fill with track's duration
    waitForTrackToFinish(duration, () => updateRoomTrack(roomName));

    // roomTrack = newTrack
    functions.logger.log('Updating track in ' + roomName);

    await set(trackQueue, queuePath(roomName));
    return set(newTrack, roomTrackPath(roomName));
}

roomTrackPath = (roomName) => '/music/' + roomName;
queuePath = (roomName) => '/queue/' + roomName;
historyPath = (roomName, trackId) => '/history/'+ roomName + '/' + trackId;

set = (data, path) => database.ref(path).set(data);

deleteP = (path) => database.ref(path).delete();

get = (path) => database.ref(path).once('value');
addSongToHistoryList = (trackInfo, roomName) => {
    // set(trackInfo, historyPath(roomName, trackInfo.name));
    set(trackInfo, historyPath(roomName, trackInfo.n));
}