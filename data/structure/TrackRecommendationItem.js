export default class TrackRecommendationItem {
    trackRecommendationItem = {};
    constructor (trackInfo, userInfo_) {
        artist = trackInfo.artists[0];
        trackImageUri = trackInfo.album.images[0].url
        userInfo = {
            name: userInfo_.display_name,
            id: userInfo_.uri
        }
        this.trackRecommendationItem = {
            id: trackInfo.id, 
            name: trackInfo.name, 
            artist: artist, 
            userInfo, 
            trackUri: trackInfo.uri,
            albumUri: trackInfo.album.uri,
            status: "Pending", 
            vote: 0,
            image: trackImageUri}
    }

    get = () => {
        return this.trackRecommendationItem;
    }
}