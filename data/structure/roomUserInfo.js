export default class RoomUserInfo {
    basicUserInfo = {}
    constructor (userInfo) {
        this.basicUserInfo = {
            id: userInfo.id,
            userUri: userInfo.uri,
            display_name: userInfo.display_name,
            isDj: userInfo.isDj,
            image: userInfo.images[0].url
        }
    }

    get = () => {
        return this.basicUserInfo;
    }
}