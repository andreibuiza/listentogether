export default class RoomTrackInfo {
    roomTrackInfo = {}
    constructor (trackInfo) {
        let trackStartTime = Date.now();
        const networkDelayBuffer = 1000;
        if (trackInfo.playbackPosition > networkDelayBuffer) {
            trackStartTime = trackStartTime  - trackInfo.playbackPosition;
        }
        this.roomTrackInfo = {
            playbackRestrictions: trackInfo.playbackRestrictions,
            playbackOptions: trackInfo.playbackOptions,
            isPaused: trackInfo.isPaused,
            track: trackInfo.track,
            playbackPosition: trackInfo.playbackPosition,
            timeTrackStarted: trackStartTime,
        }
    }

    get = () => {
        return this.roomTrackInfo;
    }
}