export default class TrackHistoryItem {
    trackHistoryItem = {};
    constructor (trackInfo, musicScore, trackImageUri) {
        const re = /spotify:track:(\w+)/;
        let id = re.exec(trackInfo.track.uri)[1];
        this.trackHistoryItem = {
            name: trackInfo.track.name,
            trackUri: trackInfo.track.uri,
            albumUri: trackInfo.track.album.uri,
            id,
            artist: trackInfo.track.artist.name,
            artistUri: trackInfo.track.artist.uri,
            score: musicScore,
            image: trackImageUri
        }
    }

    get = () => {
        return this.trackHistoryItem;
    }
}