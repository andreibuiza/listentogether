class DefaultWebApi {
    getTrackPreview = (uri) => {
        fetch(uri, {
        method: 'GET',
        headers: {
            Accept: 'audio/mpeg',
            'Content-Type': 'audio/mpeg'
        },
        }).then((response) => {
            console.log("got the mp3 file");
            console.log(response);
        });
    }
}


const defaultWebApi = new DefaultWebApi();
export default defaultWebApi;